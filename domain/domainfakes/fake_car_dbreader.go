// Code generated by counterfeiter. DO NOT EDIT.
package domainfakes

import (
	"context"
	"sync"

	"gitlab.com/jcorry/cars/domain"
)

type FakeCarDBReader struct {
	ListStub        func(context.Context) ([]domain.Car, error)
	listMutex       sync.RWMutex
	listArgsForCall []struct {
		arg1 context.Context
	}
	listReturns struct {
		result1 []domain.Car
		result2 error
	}
	listReturnsOnCall map[int]struct {
		result1 []domain.Car
		result2 error
	}
	ReadStub        func(context.Context, domain.CarID) (domain.Car, error)
	readMutex       sync.RWMutex
	readArgsForCall []struct {
		arg1 context.Context
		arg2 domain.CarID
	}
	readReturns struct {
		result1 domain.Car
		result2 error
	}
	readReturnsOnCall map[int]struct {
		result1 domain.Car
		result2 error
	}
	invocations      map[string][][]interface{}
	invocationsMutex sync.RWMutex
}

func (fake *FakeCarDBReader) List(arg1 context.Context) ([]domain.Car, error) {
	fake.listMutex.Lock()
	ret, specificReturn := fake.listReturnsOnCall[len(fake.listArgsForCall)]
	fake.listArgsForCall = append(fake.listArgsForCall, struct {
		arg1 context.Context
	}{arg1})
	stub := fake.ListStub
	fakeReturns := fake.listReturns
	fake.recordInvocation("List", []interface{}{arg1})
	fake.listMutex.Unlock()
	if stub != nil {
		return stub(arg1)
	}
	if specificReturn {
		return ret.result1, ret.result2
	}
	return fakeReturns.result1, fakeReturns.result2
}

func (fake *FakeCarDBReader) ListCallCount() int {
	fake.listMutex.RLock()
	defer fake.listMutex.RUnlock()
	return len(fake.listArgsForCall)
}

func (fake *FakeCarDBReader) ListCalls(stub func(context.Context) ([]domain.Car, error)) {
	fake.listMutex.Lock()
	defer fake.listMutex.Unlock()
	fake.ListStub = stub
}

func (fake *FakeCarDBReader) ListArgsForCall(i int) context.Context {
	fake.listMutex.RLock()
	defer fake.listMutex.RUnlock()
	argsForCall := fake.listArgsForCall[i]
	return argsForCall.arg1
}

func (fake *FakeCarDBReader) ListReturns(result1 []domain.Car, result2 error) {
	fake.listMutex.Lock()
	defer fake.listMutex.Unlock()
	fake.ListStub = nil
	fake.listReturns = struct {
		result1 []domain.Car
		result2 error
	}{result1, result2}
}

func (fake *FakeCarDBReader) ListReturnsOnCall(i int, result1 []domain.Car, result2 error) {
	fake.listMutex.Lock()
	defer fake.listMutex.Unlock()
	fake.ListStub = nil
	if fake.listReturnsOnCall == nil {
		fake.listReturnsOnCall = make(map[int]struct {
			result1 []domain.Car
			result2 error
		})
	}
	fake.listReturnsOnCall[i] = struct {
		result1 []domain.Car
		result2 error
	}{result1, result2}
}

func (fake *FakeCarDBReader) Read(arg1 context.Context, arg2 domain.CarID) (domain.Car, error) {
	fake.readMutex.Lock()
	ret, specificReturn := fake.readReturnsOnCall[len(fake.readArgsForCall)]
	fake.readArgsForCall = append(fake.readArgsForCall, struct {
		arg1 context.Context
		arg2 domain.CarID
	}{arg1, arg2})
	stub := fake.ReadStub
	fakeReturns := fake.readReturns
	fake.recordInvocation("Read", []interface{}{arg1, arg2})
	fake.readMutex.Unlock()
	if stub != nil {
		return stub(arg1, arg2)
	}
	if specificReturn {
		return ret.result1, ret.result2
	}
	return fakeReturns.result1, fakeReturns.result2
}

func (fake *FakeCarDBReader) ReadCallCount() int {
	fake.readMutex.RLock()
	defer fake.readMutex.RUnlock()
	return len(fake.readArgsForCall)
}

func (fake *FakeCarDBReader) ReadCalls(stub func(context.Context, domain.CarID) (domain.Car, error)) {
	fake.readMutex.Lock()
	defer fake.readMutex.Unlock()
	fake.ReadStub = stub
}

func (fake *FakeCarDBReader) ReadArgsForCall(i int) (context.Context, domain.CarID) {
	fake.readMutex.RLock()
	defer fake.readMutex.RUnlock()
	argsForCall := fake.readArgsForCall[i]
	return argsForCall.arg1, argsForCall.arg2
}

func (fake *FakeCarDBReader) ReadReturns(result1 domain.Car, result2 error) {
	fake.readMutex.Lock()
	defer fake.readMutex.Unlock()
	fake.ReadStub = nil
	fake.readReturns = struct {
		result1 domain.Car
		result2 error
	}{result1, result2}
}

func (fake *FakeCarDBReader) ReadReturnsOnCall(i int, result1 domain.Car, result2 error) {
	fake.readMutex.Lock()
	defer fake.readMutex.Unlock()
	fake.ReadStub = nil
	if fake.readReturnsOnCall == nil {
		fake.readReturnsOnCall = make(map[int]struct {
			result1 domain.Car
			result2 error
		})
	}
	fake.readReturnsOnCall[i] = struct {
		result1 domain.Car
		result2 error
	}{result1, result2}
}

func (fake *FakeCarDBReader) Invocations() map[string][][]interface{} {
	fake.invocationsMutex.RLock()
	defer fake.invocationsMutex.RUnlock()
	fake.listMutex.RLock()
	defer fake.listMutex.RUnlock()
	fake.readMutex.RLock()
	defer fake.readMutex.RUnlock()
	copiedInvocations := map[string][][]interface{}{}
	for key, value := range fake.invocations {
		copiedInvocations[key] = value
	}
	return copiedInvocations
}

func (fake *FakeCarDBReader) recordInvocation(key string, args []interface{}) {
	fake.invocationsMutex.Lock()
	defer fake.invocationsMutex.Unlock()
	if fake.invocations == nil {
		fake.invocations = map[string][][]interface{}{}
	}
	if fake.invocations[key] == nil {
		fake.invocations[key] = [][]interface{}{}
	}
	fake.invocations[key] = append(fake.invocations[key], args)
}

var _ domain.CarDBReader = new(FakeCarDBReader)
