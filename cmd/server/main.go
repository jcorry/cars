package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"gitlab.com/jcorry/cars/server"
	"gitlab.com/jcorry/cars/store"
	"gitlab.com/jcorry/look"
)

var (
	addr    = flag.String("addr", ":9090", "endpoint of the gRPC service")
	network = flag.String("network", "tcp", "a valid network type which is consistent to -addr")
	gwaddr  = flag.String("gwaddr", ":8080", "endpoint of the rest gateway service")
)

func main() {
	flag.Parse()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	defer func() {
		signal.Stop(quit)
		cancel()
	}()

	db := store.NewCarDB()
	s := server.NewServer(db)

	logger := look.NewZapLogger("cars-api", "v1beta1", look.Debug)
	ctx = look.CtxWithLogger(ctx, logger)

	go func() {
		if err := server.Run(ctx, *network, *addr, s); err != nil {
			return
		}
	}()

	go func() {
		opts := []runtime.ServeMuxOption{}
		if err := server.RunInProcessGateway(ctx, *gwaddr, s, opts...); err != nil {
			return
		}
	}()

	select {
	case <-quit:
		fmt.Println("quitting...")
		cancel()
	case <-ctx.Done():
		fmt.Println("quitting...")
		cancel()
	}
}
