package look

import (
	"errors"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	EnvDevelopment             = "development"
	EnvProduction              = "production"
	SentryDSN                  = "https://3a6ec7adf90e4fec97ad6bff0c87cd8c@o370634.ingest.sentry.io/6777950"
	SentryEnvKey               = "SL_SENTRY_ENVIRONMENT"
	SentryReleaseKey           = "SL_SENTRY_RELEASE"
	SentryDisableErrCaptureKey = "SL_DISABLE_ERROR_CAPTURE"
)

func NewZapLogger(component, version string, level LogLevel) *LeveledLogger {
	if ver() != "" {
		version = ver()
	}

	cfg := zap.NewProductionConfig()
	cfg.EncoderConfig = zap.NewProductionEncoderConfig()
	cfg.EncoderConfig.TimeKey = "timestamp"
	cfg.EncoderConfig.EncodeTime = func(t time.Time, encoder zapcore.PrimitiveArrayEncoder) {
		encoder.AppendString(t.Format(time.RFC3339Nano))
	}
	cfg.OutputPaths = []string{"stdout"}
	cfg.InitialFields = map[string]interface{}{
		"version":   version,
		"env":       env(),
		"component": component,
	}

	opts := []zap.Option{
		zap.AddCallerSkip(3),
	}

	if !sentryErrorCaptureDisabled() {
		// Add Sentry
		err := sentry.Init(sentry.ClientOptions{
			Dsn:         SentryDSN,
			Release:     version,
			Environment: env(),
		})
		if err != nil {
			log.Fatal("sentry initialization", err)
		}

		opts = append(opts, zap.Hooks(func(e zapcore.Entry) error {
			if e.Level == zap.FatalLevel {
				sentry.CaptureMessage(e.Message)
				sentry.Flush(1 * time.Second)
			}
			return nil
		}))
	}

	zapLogger, err := cfg.Build(opts...)

	if err != nil {
		log.Fatalf("err creating logger: %v\n", err.Error())
	}

	return &LeveledLogger{
		Logger: ZapLogger{Writer: zapLogger, Format: JSON},
		Level:  level,
	}
}

// evaluates the SL_SENTRY_ENVIRONMENT to determine whether env is set, uses that if so. Otherwise, defaults to `development`
func env() string {
	env := os.Getenv(SentryEnvKey)
	switch {
	case strings.Contains(env, "prod"):
		return EnvProduction
	case strings.Contains(env, "dev"):
		return EnvDevelopment
	default:
		return EnvDevelopment
	}
}

// evaluates the SL_SENTRY_RELEASE to determine whether sentry version is set, uses that if so. Otherwise, defaults to Version
func ver() string {
	ver := os.Getenv(SentryReleaseKey)
	if len(ver) != 0 {
		return ver
	}
	return ""
}

// evaluates the SL_DISABLE_ERROR_CAPTURE key to determine whether sentry error capturing is disabled. Defaults to true.
// Note this is _disabled_, so if true, then we will NOT capture errors with Sentry
func sentryErrorCaptureDisabled() bool {
	val := os.Getenv(SentryDisableErrCaptureKey)
	// if unset, return true
	if len(val) == 0 {
		return true
	}
	// otherwise return value
	c, err := strconv.ParseBool(val)
	if err != nil {
		return false
	}
	return c
}

type ZapLogger struct {
	Writer *zap.Logger
	Format LogFormat
}

// Error implements the LeveledLogWriter Error func with a zap logger.
func (zl ZapLogger) Error(ll LogLine) {
	zl.Writer.Error(ll.Message, ll.ZapFields()...)
}

// Debug implements the LeveledLogWriter Debug func with a zap logger.
func (zl ZapLogger) Debug(ll LogLine) {
	zl.Writer.Debug(ll.Message, ll.ZapFields()...)
}

// Info implements the LeveledLogWriter Info func with a zap logger.
func (zl ZapLogger) Info(ll LogLine) {
	zl.Writer.Info(ll.Message, ll.ZapFields()...)
}

// ZapFields converts a LogLine to a slice of zapcore.Field.
//
// Zap already has built in fields for these log line information:
// - ll.Timestamp	=> ts
// - ll.File		=> caller
// - ll.LineNumber	=> caller
func (ll LogLine) ZapFields() []zapcore.Field {
	zapFields := []zapcore.Field{
		zap.String("name", ll.Name),
		zap.String("correlation_id", ll.CorrelationID),
		zap.String("span_id", ll.SpanID),
	}

	if ll.Error != "" {
		zapFields = append(zapFields, zap.Error(errors.New(ll.Error)))
	}

	for key, val := range ll.Fields {
		zapFields = append(zapFields, zap.Any(key, val))
	}

	return zapFields
}
