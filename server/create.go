package server

import (
	"context"

	"github.com/segmentio/ksuid"
	"gitlab.com/jcorry/cars/app"
	"gitlab.com/jcorry/cars/domain"
	"gitlab.com/jcorry/cars/pb"
	"gitlab.com/jcorry/look"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Create func(context.Context, *pb.CreateRequest) (*pb.CreateResponse, error)

func NewCreate(handler app.CreateCar) Create {
	return func(ctx context.Context, req *pb.CreateRequest) (*pb.CreateResponse, error) {
		ctx, span := look.OpenSpan(ctx)
		defer span.Close()

		err := req.Validate()
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		car := domain.Car{
			ID:       domain.CarID(ksuid.New().String()),
			Make:     req.Car.GetMake(),
			Model:    req.Car.GetModel(),
			Package:  req.Car.GetPackage(),
			Color:    req.Car.GetColor(),
			Year:     int(req.Car.GetYear()),
			Category: req.Car.GetCategory(),
			Mileage:  int(req.Car.GetMileage()),
			Price:    int(req.Car.GetPrice()),
		}

		span.Info("creating car", look.Fields{"Car": car})

		err = handler(ctx, car)
		if err != nil {
			span.Error(err)
			return nil, err
		}
		return &pb.CreateResponse{}, nil
	}
}

func (s *Server) Create(ctx context.Context, req *pb.CreateRequest) (*pb.CreateResponse, error) {
	return s.CreateEndpoint(ctx, req)
}
