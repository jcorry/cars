package server

import (
	"context"

	"gitlab.com/jcorry/cars/pb"
)

type Health func(context.Context, *pb.HealthRequest) (*pb.HealthResponse, error)

func NewHealth() Health {
	return func(ctx context.Context, req *pb.HealthRequest) (*pb.HealthResponse, error) {
		return &pb.HealthResponse{
			Message: "OK",
		}, nil
	}
}

func (s *Server) Health(ctx context.Context, req *pb.HealthRequest) (*pb.HealthResponse, error) {
	return s.HealthEndpoint(ctx, req)
}
