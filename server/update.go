package server

import (
	"context"

	"gitlab.com/jcorry/cars/app"
	"gitlab.com/jcorry/cars/pb"
	"gitlab.com/jcorry/look"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Update func(context.Context, *pb.UpdateRequest) (*pb.UpdateResponse, error)

func NewUpdate(handler app.UpdateCar) Update {
	return func(ctx context.Context, req *pb.UpdateRequest) (*pb.UpdateResponse, error) {
		ctx, span := look.OpenSpan(ctx)
		defer span.Close()

		err := handler(ctx, ToDomain(req.GetCar()))
		if err != nil {
			span.Error(err, look.Fields{"Car": req.GetCar()})
			return &pb.UpdateResponse{}, status.Error(codes.Internal, err.Error())
		}

		return &pb.UpdateResponse{}, nil
	}
}

func (s *Server) Update(ctx context.Context, req *pb.UpdateRequest) (*pb.UpdateResponse, error) {
	return s.UpdateEndpoint(ctx, req)
}
