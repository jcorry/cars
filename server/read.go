package server

import (
	"context"
	"strings"

	"gitlab.com/jcorry/cars/app"
	"gitlab.com/jcorry/cars/domain"
	"gitlab.com/jcorry/cars/pb"
	"gitlab.com/jcorry/look"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Read func(context.Context, *pb.ReadRequest) (*pb.ReadResponse, error)

func NewRead(handler app.ReadCar) Read {
	return func(ctx context.Context, req *pb.ReadRequest) (*pb.ReadResponse, error) {
		ctx, span := look.OpenSpan(ctx)
		defer span.Close()

		c, err := handler(ctx, domain.CarID(req.GetID()))
		if err != nil {
			span.Error(err, look.Fields{"CarID": req.GetID()})
			if strings.Contains(err.Error(), "unable to find") {
				return nil, status.Error(codes.NotFound, err.Error())
			}
			return nil, status.Error(codes.Internal, err.Error())
		}

		return &pb.ReadResponse{Car: ToPb(c)}, nil
	}
}

func (s *Server) Read(ctx context.Context, req *pb.ReadRequest) (*pb.ReadResponse, error) {
	return s.ReadEndpoint(ctx, req)
}
