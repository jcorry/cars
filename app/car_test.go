package app_test

import (
	"testing"

	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/jcorry/cars/app"
	"gitlab.com/jcorry/cars/domain"
	"gitlab.com/jcorry/cars/domain/domainfakes"
	"gitlab.com/jcorry/look"
	"gitlab.com/jcorry/look/looktest"
)

func Test_CreateCar(t *testing.T) {
	db := &domainfakes.FakeCarDBWriter{}

	var handler app.CreateCar

	testCar := domain.Car{
		ID:       domain.CarID(ksuid.New().String()),
		Make:     "Acura",
		Model:    "TL",
		Package:  "BASE",
		Color:    "silver",
		Year:     2011,
		Category: "SEDAN",
		Mileage:  145000,
		Price:    1200000,
	}

	ctx, _ := looktest.TestableContext(look.Debug, look.JSON)

	tests := map[string]struct {
		setup func(*testing.T)
		car   domain.Car
		want  error
	}{
		"success": {
			setup: func(t *testing.T) {
				handler = app.NewCreateCar(db)
			},
			car:  testCar,
			want: nil,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			tt.setup(t)
			err := handler(ctx, tt.car)
			if err != nil {
				assert.EqualError(t, err, tt.want.Error())
			}
		})
	}
}
