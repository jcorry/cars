package store

import (
	"context"
	"fmt"
	"sync"

	"gitlab.com/jcorry/cars/domain"
)

var (
	instance CarDB
	once     sync.Once
)

type CarDB struct {
	mu     sync.Mutex
	bucket map[string]domain.Car
}

func NewCarDB() *CarDB {
	once.Do(func() {
		instance = CarDB{
			bucket: make(map[string]domain.Car),
		}
	})

	return &instance
}

func (db *CarDB) Create(ctx context.Context, c domain.Car) error {
	// if map already contains a Car of this ID, error
	if _, ok := db.bucket[string(c.ID)]; ok {
		return fmt.Errorf("db already contains Car having ID %s", c.ID)
	}
	db.mu.Lock()
	defer db.mu.Unlock()

	db.bucket[string(c.ID)] = c
	return nil
}

func (db *CarDB) Update(ctx context.Context, c domain.Car) error {
	_, err := db.Read(ctx, c.ID)
	if err != nil {
		return err
	}

	db.mu.Lock()
	defer db.mu.Unlock()

	db.bucket[string(c.ID)] = c
	return nil
}

func (db *CarDB) List(ctx context.Context) ([]domain.Car, error) {
	s := []domain.Car{}
	for _, v := range db.bucket {
		s = append(s, v)
	}
	return s, nil
}

func (db *CarDB) Read(ctx context.Context, id domain.CarID) (domain.Car, error) {
	if c, ok := db.bucket[string(id)]; ok {
		return c, nil
	}
	return domain.Car{}, fmt.Errorf("unable to find car id %s", id)
}
