package store_test

import (
	"testing"

	"github.com/segmentio/ksuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/jcorry/cars/domain"
	"gitlab.com/jcorry/cars/store"
	"gitlab.com/jcorry/look"
	"gitlab.com/jcorry/look/looktest"
)

func Test_CarDB_Operations(t *testing.T) {
	c := store.NewCarDB()

	car := domain.Car{
		ID:       domain.CarID(ksuid.New().String()),
		Make:     "Toyota",
		Model:    "4Runner",
		Package:  "SR5",
		Color:    "Black",
		Year:     2016,
		Category: "SUV",
		Mileage:  101000,
		Price:    2800000,
	}

	ctx, _ := looktest.TestableContext(look.Debug, look.JSON)

	t.Run("create a car", func(t *testing.T) {
		err := c.Create(ctx, car)
		require.NoError(t, err)
	})

	t.Run("list cars", func(t *testing.T) {
		list, err := c.List(ctx)
		require.NoError(t, err)
		require.Len(t, list, 1)
	})

	fiesta := domain.Car{
		ID:       domain.CarID(ksuid.New().String()),
		Make:     "Ford",
		Model:    "Fiesta",
		Color:    "Yellow",
		Year:     1984,
		Category: "COMPACT",
		Mileage:  387000,
		Price:    400000,
	}

	t.Run("create another car", func(t *testing.T) {
		err := c.Create(ctx, fiesta)
		require.NoError(t, err)
	})

	t.Run("list cars again", func(t *testing.T) {
		list, err := c.List(ctx)
		require.NoError(t, err)
		require.Len(t, list, 2)
	})

	t.Run("read car", func(t *testing.T) {
		mycar, err := c.Read(ctx, car.ID)
		require.NoError(t, err)
		require.EqualValues(t, mycar, car)
	})

	t.Run("update car", func(t *testing.T) {
		car.Mileage = 107000
		err := c.Update(ctx, car)
		require.NoError(t, err)
	})

	t.Run("read car again", func(t *testing.T) {
		mycar, err := c.Read(ctx, car.ID)
		require.NoError(t, err)
		require.Equal(t, 107000, mycar.Mileage)
	})

	t.Run("read car, not found", func(t *testing.T) {
		_, err := c.Read(ctx, "foo")
		require.EqualError(t, err, "unable to find car id foo")
	})
}
